#ifndef DB_CONTROLLER_H
#define DB_CONTROLLER_H

#include <QObject>
#include <QSqlDatabase>
#include <QSqlQuery>
#include "authcontroller.h"

class db_controller : public QObject
{
    Q_OBJECT
public:
    explicit db_controller(QObject *parent = nullptr);
    QSqlDatabase db;
//    void sendQuieries(void);
//    void addFriends(QList<FriendsObject> model, QString dbName);
    ~db_controller();
signals:

public slots:
    void initTable(QString tableName);
};

#endif // DB_CONTROLLER_H
