#ifndef MESSAGECONTROLLER_H
#define MESSAGECONTROLLER_H

#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>
#include <QFile>
#include <QDataStream>
#include <QStringList>
#include <QAbstractListModel>

class msgObject { // объект сообщения
    public:
        msgObject (const QString &msg,
                       const bool &from);


        QString msg() const;
        bool from() const;
    private:
        QString ob_msg;
        bool ob_from;
};


class msgModel : public QAbstractListModel { // абстрактная модель как и в authcontroller
    Q_OBJECT
public:
    enum DataRoles {
        msgRole,
        fromRole
    };

    msgModel(QObject *parent = 0);

    void addMsg(const msgObject & newFriend); // функция добавления сообщения
    int rowCount(const QModelIndex & parent = QModelIndex()) const; // функция для подсчета количества сообщений
    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const; // функция для возвращения данных из c++ в qml
protected:
    QHash<int, QByteArray> roleNames() const; // функция для прописывания имени ролей
private:
    QList<msgObject> ob_msgs;

};


class messagecontroller : public QObject
{
    Q_OBJECT
public:
    explicit messagecontroller(QObject *parent = nullptr);
    QTcpServer * serv; // переменная сервера
    QTcpSocket * client_sock; // сокет имитирующий соединение со стороны клиента
    QTcpSocket * server_sock; // сокет сервера
    msgModel messages; // само сообщение

signals:

public slots:

    void newConnection(); // функция нового соединения
    void sendMessage(QString message); // функция отправки сообщения
    void read_client_Message(); // прочитать сообщение из сокета клиента
    void read_serv_Message(); // прочитать сообщение из сокета сервера
};

#endif // MESSAGECONTROLLER_H
