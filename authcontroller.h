#ifndef AUTHCONTROLLER_H
#define AUTHCONTROLLER_H

#include <QObject>
#include <QDebug>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QUrl>
#include <QEventLoop>
#include <QStringList>
#include <QAbstractListModel>
#include <QThread>
#include <dbcontroller.h>



class FriendsObject { // описание объекта друга вк
    public:
        FriendsObject (const QString &FriendId, //сам объект, который имеет 4 параметра id, имя, ссылку на фото и статус
                       const QString &FriendName,
                       const QString &Photo,
                       const QString &Status);

        QString FriendId() const; // создание функций для возврата переменных параметров соответственно
        QString FriendName() const;
        QString Photo() const;
        QString Status() const;
    private:
        QString ob_friendid;   // создание самих переменных параметров, которые будут возвращаться
        QString ob_friendname;
        QString ob_photo;
        QString ob_status;
};


class FriendsModel : public QAbstractListModel { // создание класса абстрактной модели друзей
    Q_OBJECT
public:
    enum DataRoles { // перечисление и объявление ролей для связи qml и c++
        FriendIdRole,
        FriendNameRole,
        PhotoRole,
        StatusRole
    };

    FriendsModel(QObject *parent = 0);

    void addFriend(const FriendsObject & newFriend); // функция добавления друга в список друзей (в модель)
    int rowCount(const QModelIndex & parent = QModelIndex()) const; // функция для подсчета количества друзей в списке
    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const; // функция для возвращения данных из переменных объекта друга в qml
protected:
    QHash<int, QByteArray> roleNames() const; // описание ролей
private:
    QList<FriendsObject> ob_friends; // список объектов друзей

};


class AuthController : public QObject
{
Q_OBJECT
public:
dbcontroller datBase;
FriendsModel friendsModel;
QNetworkAccessManager * na_manager= new QNetworkAccessManager(this);
QNetworkReply * reply;
QNetworkRequest request;
QString response;
QString access_token;
QString client_id;
explicit AuthController(QObject *parent = nullptr);
signals:
authsuccess();

public slots:
//параметры такие же, как и в сигнале
void Authentificate(QString login,QString password);
};
#endif // AUTHCONTROLLER_H
